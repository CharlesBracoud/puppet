Class dokuwiki{
    #$source_path = '/usr/src'
    #$binary_path = '/usr/bin'
    #$web_path = '/var/www'

  package {
    'apache2':
      ensure => present,
      name   => 'apache2',
  }

  package {
    'php7.3':
      ensure => present,
      name   =>'php7.3',
  }



  file {
    'download dokuwiki':
      ensure => present,
      path   => '/usr/src/dokuwiki.tgz',
      source => 'https://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz>'
  }

  exec {
    'unzip dokuwiki':
      command => 'tar xavf dokuwiki.tgz',
      path    => ['/usr/bin'],
      cwd     => '/usr/src',
      require => File['download dokuwiki']
      unless  => 'test -d /usr/src/dokuwiki-2020-07-29/'
  }

  file {
    'rename dokuwiki':
      ensure  => present,
      path    => '/usr/src/dokuwiki',
      source  => '/usr/src/dokuwiki-2020-07-29',
      recurse => true,
      require => Exec['unzip dokuwiki']
  }
}





define deploy($name,$documentRoot, $source, $group) {
  file {
    'deploy my site: ${name}':
      ensure  => directory,
      path    => $documentRoot,,
      source  => $source,
      recurse => true,
      owner   => $group,
      group   => $group,
      require => File['rename dokuwiki']
  }
}

node server0 {
#  $siteName = "politique.wiki"
  include dokuwiki
  #include dokuwiki_deploy
  deploy('politique.wiki', '/usr/src/dokuwiki', 'www-data')
  deploy('tajineworld.com', '/usr/src/dokuwiki', 'www-data')
}

node server1 {
#  $siteName = "recettes.wiki"
  include dokuwiki
  #include dokuwiki_deploy
  deploy('recettes.wiki', '/usr/src/dokuwiki', 'www-data')
}

#class dokuwiki_deploy {
#    file { "create new directory for ${env}.wiki in ${web_path} and allow apac>
#            ensure  => directory,
#            path    => "${web_path}/${env}.wiki",
#            recurse => true,
#            owner   => 'www-data',
#            group   => 'www-data',
#            require => File['rename dokuwiki']
#    }
#}


